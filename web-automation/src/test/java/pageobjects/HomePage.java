package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class HomePage extends BaseClass {
    By filter = new By.ByXPath("//select[@class='product_sort_container']");
    By pageHeading = new By.ByXPath("//div[contains(text(),'Products')]");

    public HomePage(WebDriver driver) {
        super(driver);
    }
    //apply filter
    public void applyFilter(String value) {
        driver.findElement(By.xpath("//*[@id=\"inventory_filter_container\"]/select/option[3]")).click();
        Select sorter = new Select(driver.findElement(By.xpath("//select[@class =\"product_sort_container\"]")));
        sorter.selectByValue(value);
    }
    //get the filter value
    public String getAppliedFilter() {
        return driver.findElement(By.xpath("//*[@id=\"inventory_filter_container\"]/select")).getText();
    }

    public String getPageHeading() {
        return driver.findElement(pageHeading).getText();
    }
    //go to a product detail page
    public void clickOnProductName(String productName){
        driver.findElement(By.xpath(XPATH.getProperty("PRODUCT_NAME"))).click();
    }
    //add to cart locator using index
    public WebElement getAddToCart(int index){
        return driver.findElement(By.xpath(XPATH.getProperty("ADD_TO_CART")));
    }
    //add a product to cart
    public void clickOnAddToCart(int index){
        this.getAddToCart(index).click();
    }
    //remove from cart locator using index
    public WebElement getRemoveFromCart(int index){
        return driver.findElement(By.xpath(XPATH.getProperty("REMOVE_FROM_CART")));
    }
    //remove a product from cart
    public void clickOnRemoveFromCart(int index){
        this.getRemoveFromCart(index).click();
    }
}