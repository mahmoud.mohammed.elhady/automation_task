package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.ProductPage;

public class HomePageTests extends BaseTest{
    HomePage homePage;
    LoginPage loginPage;

    @BeforeMethod
    public void beforeEach(){
        driver.get(properties.getProperty("URL"));
        loginPage = new LoginPage(driver);
        loginPage.login(properties.getProperty("username"), properties.getProperty("password"),false);
        homePage = new HomePage(driver);
    }
    @Test(description = "Verify product should be added to cart successfully")
    public void addToCart(){
        Assert.assertTrue(homePage.getAddToCart(3).isDisplayed());
        homePage.clickOnAddToCart(3);
        Assert.assertTrue(homePage.getRemoveFromCart(3).isDisplayed());
        Assert.assertEquals(homePage.getCartCount(), 1);
        homePage.clickOnAddToCart(4);
        Assert.assertEquals(homePage.getCartCount(), 2);
        homePage.resetAppState();
    }
    @Test(description = "Verify product should be removed from cart successfully")
    public void removeFromCart(){
        homePage.clickOnAddToCart(3);
        homePage.clickOnAddToCart(2);
        Assert.assertEquals(homePage.getCartCount(), 2);
        homePage.clickOnRemoveFromCart(2);
        Assert.assertEquals(homePage.getCartCount(), 1);
        homePage.resetAppState();
    }
}