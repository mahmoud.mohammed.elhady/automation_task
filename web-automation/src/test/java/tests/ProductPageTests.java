package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.ProductPage;

public class ProductPageTests extends BaseTest{
    HomePage homePage;
    LoginPage loginPage;
    ProductPage productPage;
    @BeforeMethod
    public void beforeEach(){
        driver.get(properties.getProperty("URL"));
        loginPage = new LoginPage(driver);
        loginPage.login(properties.getProperty("username"), properties.getProperty("password"),false);
        homePage = new HomePage(driver);
        homePage.clickOnProductName(properties.getProperty("product_name"));
        productPage = new ProductPage(driver);
    }
    @Test(description = "Verify add to cart should work on product page successfully")
    public void addToCartProductPage() {
        productPage.getAddToCartButton().click();
        Assert.assertEquals(homePage.getCartCount(), 1);
        productPage.resetAppState();
    }
}